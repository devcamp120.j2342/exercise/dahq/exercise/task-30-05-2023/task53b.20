import model.Circle;
import model.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "Green");
        System.out.println("Circle1: " + circle1.toString());
        System.out.println("Circle2: " + circle2.toString());
        System.out.println("Circle3: " + circle3.toString());
        System.out.println("Dien tich circle2: " + circle2.getArea());
        System.out.println("Dien tich circle3: " + circle3.getArea());

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        Cylinder cylinder4 = new Cylinder(3.5, "Green", 1.5);

        System.out.println("cylinder1: " + cylinder1.toString());
        System.out.println("Cylinder2: " + cylinder2.toString());
        System.out.println("Cylinder3:" + cylinder3.toString());
        System.out.println("Cylinder4: " + cylinder4.toString());
        System.out.println("The Tich C1: " + cylinder1.getVolume());
        System.out.println("The Tich C2: " + cylinder2.getVolume());
        System.out.println("The Tich C3: " + cylinder3.getVolume());
        System.out.println("The Tich C4: " + cylinder4.getVolume());

    }
}
